# Changes Log

## 0.1.3

* Minor changes.

## 0.1.2

* Fix broken `reportStream` listener (pull request [#1](https://bitbucket.org/pinchprojectbackend/node-autoingest/pull-request/1/fix-broken-reportstream-listener) from [matt-holden](https://bitbucket.org/matt-holden)).

## 0.1.1

* Minor changes.

## 0.1.0

*	First release.