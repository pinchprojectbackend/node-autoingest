/**
 * Author: Ismael Gorissen
 * Date: 09/08/13 17:03
 * Company: PinchProject
 */

var fs = require('fs'),
    path = require('path');

var CONFIG_FILES_PATH = path.join(__dirname, '..', 'config');

var cache = {};

/**
 *
 * @param fileName
 * @returns {*}
 */
exports.loadSync = function (fileName) {
    if (!cache[fileName]) {
        var filePath = path.join(CONFIG_FILES_PATH, fileName + '.json');
        cache[fileName] = JSON.parse(fs.readFileSync(filePath, {encoding: 'utf8'}));
    }

    return cache[fileName];
};

/**
 *
 * @param fileName
 * @param callback
 * @returns {*}
 */
exports.load = function (fileName, callback) {
    if (cache[fileName]) return callback(null, cache[fileName]);

    var filePath = path.join(CONFIG_FILES_PATH, fileName + '.json');

    fs.readFile(
        filePath,
        {encoding: 'utf8'},
        function (err, data) {
            if (err) return callback(err);

            cache[fileName] = JSON.parse(data);

            callback(null, cache[fileName]);
        }
    );
};