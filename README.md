# node-autoingest

[![NPM](https://nodei.co/npm/autoingestion.png?downloads=true&stars=true)](https://nodei.co/npm/autoingestion/)

[![NPM](https://nodei.co/npm-dl/autoingestion.png?months=9)](https://nodei.co/npm/autoingestion/)

## Description

Apple **Auto-Ingest** tool write in JavaScript for Node.js. This tool for now is written in Java. So we have decided to rewrite this tool in a Node.js module to benefit from the asynchronous world that is Node.js.

## Prerequesites

Download the [iTunes Connect Sales And Trends Guide Apps](http://www.apple.com/itunesnews/docs/AppStoreReportingInstructions.pdf) PDF file.

## How To

### Install

```
$ npm install autoingestion
```

### Use

#### Add required module.

```js
var AutoIngestion = require('autoingestion');
```

#### Create a JSON with all required parameters.

```js
var parameters = {
	username: 'ITUNES_CONNECT_USERNAME',
	password: 'ITUNES_CONNECT_PASSWORD',
	vendor_number: 'ITUNES_CONNECT_VENDOR_NUMBER',
	report_type: 'SALES/NEWSSTAND/OPT-IN',
	report_subtype: 'SUMMARY/DETAILED',
	date_type: 'DAILY/WEEKLY/MONTHLY/YEARLY',
	report_date: 'DAILY=YYYYMMDD/MONTHLY=YYYYMM/YEARLY=YYYY'
};
```

**IMPORTANT** :

* All parameters are required but normaly only the `report_date` is optionnal.

#### Create a JSON with the paths where the archive will be download and extract.

```js
var paths = {
	archive: 'PATH_WHERE_ARCHIVE_WILL_BE_DOWNLOADED',
	report: 'PATH_WHERE_ARCHIVE_WILL_BE_EXTRACTED'
};
```

**IMPORTANT** :

* All parameters are required.

#### Call the downloadReportWith method with th previously created JSONs and a callback.

```js
AutoIngestion.downloadReportWith(
	parameters,
	paths,
	function (err, filePaths) {
		if (err && (err instanceof AutoIngestion.INVALID_PARAMETERS_ERROR || err instanceof AutoIngestion.INVALID_PATHS_ERROR)) return console.log(err);
		if (err && err instanceof AutoIngestion.INVALID_FILE_SIZE_ERROR) return console.log(err);
		if (err) return console.log(err);

		console.log(filePaths);
	}
);
```

**IMPORTANT** :

* You can have an `INVALID_PARAMETERS_ERROR` or `INVALID_PATHS_ERROR` when there is a issue with the parameters or paths JSON.
* Paths that do not exist will be created.
* If there is no error, the archive and report file paths will be returned as a JSON.
* If you try to download a report that have not been generated yet by Apple, you will receive an `INVALID_FILE_SIZE_ERROR` because the module have downloaded an empty file. This empty file will be removed automatically.

## MIT License

See LICENSE.md file.

## Change Log

See CHANGE_LOG.md file.
