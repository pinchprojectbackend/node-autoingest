/**
 * Author: Ismael Gorissen
 * Date: 09/08/13 18:14
 * Company: PinchProject
 */

var util = require('util');
var AbstractError = require('./AbstractError.js');

util.inherits(InvalidPathsError, AbstractError);

function InvalidPathsError(message) {
    AbstractError.call(this, message, this.constructor);
    this.name = 'Invalid Paths Error';
}

module.exports = InvalidPathsError;