/**
 * Author: Ismael Gorissen
 * Date: 10/08/13 12:47
 * Company: PinchProject
 */

var util = require('util');

util.inherits(AbstractError, Error);

function AbstractError(message, constr) {
    Error.captureStackTrace(this, constr || this);
    this.message = message || 'Error';
}

AbstractError.prototype.name = 'Abstract Error';

module.exports = AbstractError;