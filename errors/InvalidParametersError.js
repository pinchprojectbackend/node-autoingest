/**
 * Author: Ismael Gorissen
 * Date: 09/08/13 17:42
 * Company: PinchProject
 */

var util = require('util');
var AbstractError = require('./AbstractError.js');

util.inherits(InvalidParametersError, AbstractError);

function InvalidParametersError(message) {
    AbstractError.call(this, message, this.constructor);
    this.name = 'Invalid Parameters Error';
}

module.exports = InvalidParametersError;